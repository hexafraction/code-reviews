#include "metastat.h"

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Error: there should be exactly one argument\n\nUsage: metastat PATHNAME\n");
        exit(-1);
    }

    DIR* start;
    if (!(start = try_open_dir(argv[1]))) {
        fprintf(stderr, "\nError: starting path invalid\n");
        exit(-1);
    }
    // array for keeping list of hardlinked inodes
    hardlinked_inodes = (struct inode_tracking*) malloc(sizeof(struct inode_tracking));
    hardlinked_inodes->size = 16; // allow 16 hard linked non-dir inodes to start
    hardlinked_inodes->unique_inodes = (struct unique_inode*) malloc(hardlinked_inodes->size * sizeof(struct unique_inode));
    // begin recursive descent
    get_meta_stats(start, argv[1]);
    free(hardlinked_inodes->unique_inodes);
    free(hardlinked_inodes);

    for (int i = 0; i<81; i++) printf("-");
    printf("\ninode count:\n regular files: %lu\n dirs: %lu\n char devs: %lu\n block devs: %lu\n FIFOs: %lu\n symlinks: %lu\n sockets: %lu\n", 
        stats.inode_count_file, stats.inode_count_dir, stats.inode_count_char, stats.inode_count_blk,
        stats.inode_count_fifo, stats.inode_count_symlink, stats.inode_count_socket);
    printf(" regular files:\n  total size: %lu\n  total blocks: %lu\n", stats.total_size, stats.total_blocks);
    printf("\nnumber of inodes (excluding directories) with nlink > 1: %lu\n", stats.inode_linksgt1_count - stats.inode_count_dir);
    printf("number of symlinks that do not resolve to a valid path: %lu\n", stats.invalid_symlink_count);
    printf("number of directory entries with problematic characters: %lu\n", stats.problematic_count);

    return 0;
}

// caller must handle null return and must free
DIR* try_open_dir(char* path) {
    DIR* directory;
    if(!(directory = opendir(path))) {
        fprintf(stderr, "Unable to access directory \"%s\": %s\n-----> Continuing...\n", path, strerror(errno));
        return NULL;
    }
    return directory;
}

static void get_meta_stats(DIR* directory, char* this_dir_path) {
    struct dirent* entry; struct stat entry_stats;
    char pathname[4097] = {0}; // could be cleaner but PATH_MAX is controversial
    while (1) { // looping through directory entries
        errno = 0;
        if (!(entry = readdir(directory))) {
            if (errno) {
                fprintf(stderr, "NOTE: Unable to access directory entry: %s\n-----> Continuing...\n", strerror(errno));
                continue;
            } else break; // end of directory
        }
        // get path relative to starting dir
        snprintf(pathname, sizeof(pathname), "%s/%s", this_dir_path, entry->d_name);
        if (lstat(pathname, &entry_stats) < 0) {
            printf("Unable to get statistics for file: %s: %s\n", pathname, strerror(errno));
            continue;
        }
        // recurse into directory
        if ((S_ISDIR(entry_stats.st_mode) && strcmp(entry->d_name, "..") && strcmp(entry->d_name, "."))) {
            DIR* next_directory;
            if ((next_directory = try_open_dir(pathname))) {
                get_meta_stats(next_directory, pathname);
            }
        }
        detect_hardlinked_inode(&entry_stats, pathname);
        // check for problematic chars
        check_bad_characters(entry->d_name);
    }
    closedir(directory);
}

static void detect_hardlinked_inode(struct stat* entry_stats_p, char* pathname) {
    if ((entry_stats_p->st_nlink > 1)) {
    if (stats.inode_linksgt1_count >= hardlinked_inodes->size) {
        struct unique_inode* new_hl_array;
        hardlinked_inodes->size *= 2;
        if (!(new_hl_array = realloc(hardlinked_inodes->unique_inodes, (hardlinked_inodes->size)*sizeof(struct unique_inode)))) {
            fprintf(stderr, "Error allocating memory\n");
            free(hardlinked_inodes);
            exit(-1);
        }
        hardlinked_inodes->unique_inodes = new_hl_array;
    }
    int present = 0;
    for (uint64_t i = 0; i<stats.inode_linksgt1_count; i++) { // search hardlinked inode list
        if (hardlinked_inodes->unique_inodes[i].inode_num == entry_stats_p->st_ino && hardlinked_inodes->unique_inodes[i].device_num == entry_stats_p->st_dev) {
            present = 1;
            break;
        }
    }
    if (!present) { // we found a new non-dir inode
        hardlinked_inodes->unique_inodes[stats.inode_linksgt1_count].inode_num = entry_stats_p->st_ino;
        hardlinked_inodes->unique_inodes[stats.inode_linksgt1_count].device_num = entry_stats_p->st_dev;
        set_inode_counts(entry_stats_p, pathname);
        stats.inode_linksgt1_count++;
    }
    } else {
        set_inode_counts(entry_stats_p, pathname);
    }
}

static void set_inode_counts(struct stat* entry_stats_p, char* pathname) {
        if (S_ISDIR(entry_stats_p->st_mode))   stats.inode_count_dir++;
        if (S_ISCHR(entry_stats_p->st_mode))   stats.inode_count_char++;
        if (S_ISBLK(entry_stats_p->st_mode))   stats.inode_count_blk++;
        if (S_ISFIFO(entry_stats_p->st_mode))  stats.inode_count_fifo++;
        if (S_ISSOCK(entry_stats_p->st_mode))  stats.inode_count_socket++;
        if (S_ISREG(entry_stats_p->st_mode)) {
            stats.inode_count_file++;
            stats.total_size   += (uint64_t)entry_stats_p->st_size;
            stats.total_blocks += (uint64_t)entry_stats_p->st_blocks;
        }
        if (S_ISLNK(entry_stats_p->st_mode)) {
            struct stat dont_care; // for stat() so we can check if symlink resolves to a valid path
            stats.inode_count_symlink++;
            if (stat(pathname, &dont_care) < 0) stats.invalid_symlink_count++;
        }
}

static void check_bad_characters(char* filename) {
    static const char bad_chars[] = "\"#$&\'()*-/\\<>?`!~|"; // printable but still bad
    for (int i=0; i < (int)(strlen(filename)); i++) {  
        if (!(isprint(filename[i]) || isspace(filename[i]))) break;
        for (int j = 0; j < (int)(sizeof(bad_chars) / sizeof(char)); j++) {
            if (!((filename[i] != bad_chars[j]))) {
                stats.problematic_count++;
                return;
            }
        }
    }
}
