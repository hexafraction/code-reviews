#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void cat_single_file(char* infile, int out_fd);

int main(int argc, char **argv) {
    int out_fd = STDOUT_FILENO;

    //corner case: no arguments
    if (argc==1) {
        cat_single_file("-", out_fd);
        exit(0);
    }

    //see if we have custom output file specified & open it if needed
    if (!strcmp(argv[1], "-o")) {
        if (argc<3) {
            fprintf(stderr, "Missing required output file argument for flag \"-o\".\nUsage: kitty [-o outfile] infile1 [...infile2....]\n       kitty [-o outfile]");
            exit(-1);
        }
        out_fd = openat(AT_FDCWD, argv[2], O_CREAT|O_TRUNC|O_WRONLY, 0666);
        if (out_fd==-1) {
            fprintf(stderr, "Error encountered while opening file \"%s\" for writing: %s\n", argv[2], strerror(errno));
            exit(-1);
        }
    }
    //concatenate each remaining input file to the output file
    for (int i=(out_fd==1 ? 1 : 3); i<argc; i++) {
        cat_single_file(argv[i], out_fd);
    }

    if(out_fd!=STDOUT_FILENO && (close(out_fd) < 0)) {
        fprintf(stderr, "Error closing output file\n");
        exit(-1);
    }
}

void cat_single_file(char* infile, int out_fd) {
    int in_fd = STDIN_FILENO;
    char readbuf[4096];
    //open the input file for reading if it's not standard input
    if(strcmp(infile, "-")) {
        if ((in_fd = openat(AT_FDCWD, infile, O_RDONLY)) < 1) {
            fprintf(stderr, "Error encountered while opening file \"%s\" for reading: %s\n", infile, strerror(errno));
            exit(-1);
        }
    }
    //read into the buffer and write out from the buffer
    ssize_t read_result, write_result;
    ssize_t bytes_read = 0, read_syscalls = 0, write_syscalls=0;
    int is_binary = 0;
    while (1) {
        if ((read_result = read(in_fd, readbuf, 4096)) < 0) {
            fprintf(stderr, "Error encountered while reading the file \"%s\": %s\n", infile, strerror(errno));
            exit(-1);
        } else if (read_result == 0) break;
        bytes_read += read_result;
        read_syscalls++;

        for (int i=0; i<read_result; i++) {      //check if file is binary
            if (!(isprint(readbuf[i])||isspace(readbuf[i]))) {
                is_binary=1;
                break;
            }
        }

        if ((write_result = write(out_fd, readbuf, (size_t)read_result)) < 0) {
            fprintf(stderr, "Error encountered while writing to the file \"%s\": %s\n", infile, strerror(errno));
            exit(-1);
        }
        write_syscalls++;

        //catch and try to fix partial writes
        if (write_result != read_result) {
            fprintf(stderr, "WARNING: Partial write detected (will try to handle)\n");
            long write_bytes_remaining = read_result-write_result;
            int retry_count = 0;
            while (1) {
                write_result = write(out_fd, readbuf, (size_t)(write_bytes_remaining));
                if (write_result<0) {
                    fprintf(stderr, "Error encountered while writing (after partial write retry) to the file \"%s\": %s\n", infile, strerror(errno));
                    exit(-1);
                }
                if ((write_bytes_remaining-=write_result) == 0) break;
                write_syscalls++;
                if (retry_count++==100) {
                    fprintf(stderr, "Error: write retry to \"%s\" failed after 100 attempts\n", infile);
                    exit(-1);
                }
            }
        }

    }
    fprintf(stderr, "\nFile: %s | Transferred: %zd bytes | Read/write syscalls: %zd/%zd\n", (in_fd==0 ? "<standard input>" : infile), bytes_read, read_syscalls, write_syscalls);
    if (is_binary) fprintf(stderr, "--> WARNING: binary file detected\n");
    if(infile!=STDIN_FILENO && (close(in_fd) < 0)) {
        fprintf(stderr, "Error closing file %s\n", infile);
        exit(-1);
    }
}
