def inator(a,b):
    for n in range(a,b+1):
        engreps = {1: "one", 2: "two", 3:"three", 4:'four', 5:'five', 6:'six', 7:'seven', 8:'eight', 9:'nine'}
        if n in engreps:
            print(engreps[n])
        elif n not in engreps:
            if n % 2 == 0:
                print ("even")
            else:
                print ("odd")