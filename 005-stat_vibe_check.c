#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>

#define INODE_BUFF 4096

void expDir(DIR *, const char *);
void get_stats(const char *, const char *, struct stat);
void print_err(const char *, const char *);
int check_inodes(struct stat);
void get_reg_size(struct stat);

static size_t tot_size;
static size_t disk_blocks;
static size_t ino_g1;   //inodes with links greater than 1
static size_t syms_to_nowhere; static size_t problematic;
static size_t inodes;

int main(int argc, char **argv){
    if(argc != 2){ 
        printf("usage: %s <pathname>\n", argv[0]); 
        exit(-1);
    }

    DIR *dir;
    if((dir = opendir(argv[1])) == 0)
        print_err("***ERROR*** Problem with inputfile %s\n", argv[1]);
    else
        expDir(dir, argv[1]);

    printf("\nInodes encountered: %10ld\nReg File Size: %15ld\nDisk Blocks: %17ld\n", inodes, tot_size, disk_blocks);
    printf("Inodes With link > 1: %8ld\nSym Links to nowhere: %8ld\nProblematic filenames: %7ld\n", ino_g1, syms_to_nowhere, problematic);
    return 0;
}

void expDir(DIR *dir, const char *pathname){ //explore directory
    struct stat statbuf;
    if(lstat(pathname, &statbuf) == -1)
        print_err("***ERROR*** Trouble getting stat for %s\n", pathname);

    __ino_t cur_dir_ino = statbuf.st_ino; //inode of current directory
    
    errno = 0;
    struct dirent *dirp;
    while( (dirp = readdir(dir)) )
    {
        int errno_save = errno;
        char path[PATH_MAX];
        snprintf(path, PATH_MAX, "%s/%s", pathname, dirp->d_name); //create a pathname to the dirent

        if(lstat(path, &statbuf) == -1)
            print_err("***ERROR*** Trouble getting stat for %s\n", pathname);

        if(errno_save == ENOENT){
            print_err("***ERROR*** %s DOES NOT EXIST\n", dirp->d_name);
            continue;
        } else if(errno_save == EACCES){
            print_err("***ERROR*** PERMISSION DENIED TO %s || INODE STILL COUNTED\n", dirp->d_name);
            get_stats(path, dirp->d_name, statbuf);
            break;
        }
            
        if(strcmp(dirp->d_name, "..") && strcmp(dirp->d_name, ".")) //ignore . and ..
        {   

//            printf("DIRENT %s\n\tDirent Inode: %lu\n\tCurrent Dir Ino: %lu\n", path, statbuf.st_ino, cur_dir_ino); //DEBUGGING
            get_stats(path, dirp->d_name, statbuf);

            if(dirp->d_type == DT_DIR && !(statbuf.st_ino == 1 || statbuf.st_ino == 2)) // if dirent is a dir and not a mount point
            {
                errno = 0;
                DIR *next_dir = opendir(path);
                if(next_dir == NULL && errno != 0){
                    if(errno == EACCES)
                        print_err("***ERROR*** PERMISSION DENIED TO DIRECTORY %s\n", path);
                    else
                        print_err("***ERROR*** PROBLEM OPENING DIRENT: %s\n", path);

                    continue;
                }

                expDir(next_dir, path);
            }
        }
        errno = 0;
    }
    if(dirp == 0 && errno != 0)
        print_err("***ERROR*** problem reading entry %s\n", dirp->d_name);

    closedir(dir);
}

void get_stats(const char *pathname, const char *name, struct stat statbuf){

    if( (statbuf.st_mode & S_IFMT) == S_IFLNK ){
        if( access(pathname, O_RDONLY) == -1 ){  //dereferences symlink and checks if the entry exists
            if(errno == ENOENT)  
                syms_to_nowhere++;       
            else{
                print_err("***ERROR OPENING SYMLINK*** %s\n", pathname);
            }
        }

    }
 
    if(statbuf.st_nlink > 1 && (statbuf.st_mode & S_IFMT) != S_IFDIR){
        if(!check_inodes(statbuf)){
            inodes++ && ino_g1++;  
            get_reg_size(statbuf);
        }
    } else { 
        inodes++;
        get_reg_size(statbuf);
    }

    /* iterate through name of dirent and check if it contains problematic characters  */
    const char shell_prob_chars[] = "+-~!;\"#$&'*/<>\\`|"; //+ - ~ ! are problematic if they are first char of a file name, else fine
    for(int i = 0; i < strlen(name); i++){
        if( !isalnum(name[i])){
            for(int j = 3*(i != 0); j < sizeof(shell_prob_chars); j++)
                if(name[i] == shell_prob_chars[j] || !(isprint(name[i]) || isspace(name[i]) )){
                    problematic++;
                    return;
                }
        }
    }
}

int check_inodes(struct stat statbuf){
    static __ino_t counted_inodes[INODE_BUFF] = {1,2}; 
    static int count = 2;
    for(int index = 0; index < count; index++)
        if(statbuf.st_ino == counted_inodes[index]) 
            return 1; //inode was counted
            
    counted_inodes[count++] = statbuf.st_ino;
    return 0; //inode had not been counted
}

void get_reg_size(struct stat statbuf){ // checks if inode is a regular file and counts the size and diskblocks used
    if((statbuf.st_mode & S_IFMT) == S_IFREG){
        tot_size += statbuf.st_size;
        disk_blocks += statbuf.st_blocks*(512.0/statbuf.st_blksize); // get the amount of disk_blocks being used by the file
    }
}

void print_err(const char *err_msg, const char *file){
    int errno_save = errno;
    fprintf(stderr, err_msg, file);
    if(errno_save != 0)
        fprintf(stderr, "(errno %d) %s\n", errno_save, strerror(errno_save));
}
