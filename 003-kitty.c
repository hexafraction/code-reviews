#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#define BUFFER_SIZE 4096

char buff[BUFFER_SIZE];
int r_w_calls = 0;

void err_n_die(const char *, const char *); 
ssize_t concate(const char *, int, int);

int main(int argc, char **argv){
    int fd_infile = 0;
    int fd_outfile = 1;  
    ssize_t  tbytes = 0;    //totalBytes
    char *outputfile = "<standard output>";

    //get the output file
    for(int i = 0; i < argc; i++)
    {
        if(!strcmp(argv[i], "-o")){
            if((i + 1) < argc){
                outputfile = argv[i+1];
                if((fd_outfile = open(outputfile, O_WRONLY | O_TRUNC | O_CREAT, 0666)) == -1)
                    err_n_die("error opening outfile (%s)\n", outputfile);
                break;
             }else
                err_n_die("%s: please specify a valid output file!!!(remember -o specifies the output file)\n", argv[0]);
        }
    }

    int index = 1;      
    do {
        if(argc == 1 || argc == 3){
            fd_infile = 0;
        }else if(!strcmp(argv[index], "-")){ // - reads input from stdin
            fd_infile = 0;
        }else if(!strcmp(argv[index], "-o")){
            index += 1;
            continue;
        }else if((fd_infile = open(argv[index], O_RDONLY)) == -1){ //else read from specified file
            err_n_die("trouble reading from (%s)\n", argv[index]);
        }            

        tbytes += concate(argv[index], fd_infile, fd_outfile);

        if(fd_infile != STDIN_FILENO){
            if(close(fd_infile) == -1) 
                err_n_die("error closing (%s)\n", argv[index]);
        }
    } while(++index < argc);
    
    close(fd_outfile); 
    fprintf(stderr, "output file: %s\n", outputfile);
    fprintf(stderr, "bytes: %ld\nRead/Write calls: %d\n", tbytes, r_w_calls);
    return 0;
}

ssize_t concate(const char *cur_file, int fd_infile, int fd_outfile){
    ssize_t tbytes = 0, rbytes = 0;    
    while((rbytes = read(fd_infile, buff, BUFFER_SIZE)) > 0){
        ssize_t wbytes = 0;
        // check if input is from a binary file
        for(int i = 0; i < rbytes; i++){ 
            if( !(isprint(buff[i]) || isspace(buff[i])) ){
                fprintf(stderr, "***WARNING*** %s is a binary file\n", cur_file);
                break;
            }
        }

        while(rbytes > (wbytes += write(fd_outfile, buff, rbytes-wbytes))){
            fprintf(stderr, "***WARNING*** PARTIAL WRITE DETECTED FOR %s\n", cur_file);
            for(int i = 0; i < (rbytes - wbytes); i++){
                buff[i] = buff[i + (rbytes - wbytes)];
            }
        }
        tbytes += wbytes;
        r_w_calls += 2;
    }

    return tbytes;
}

//reports errors and exits the process
void err_n_die(const char *err_msg, const char *prob_file){
    int errno_save = errno;
    fprintf(stderr, err_msg, prob_file);

    if(errno_save != 0)
        fprintf(stderr, "(errno: %d) %s\n", errno_save, strerror(errno_save));

    exit(-1);
}
